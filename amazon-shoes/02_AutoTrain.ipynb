{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e8240cba-a217-4424-a710-c475d5e036da",
   "metadata": {},
   "source": [
    "# Training models automatically with AutoTrain\n",
    "\n",
    "In this notebook, you'll walk through the [AutoTrain](https://huggingface.co/autotrain) user interface to train models automatically on the Amazon reviews dataset we prepared in ```01_data_prep```."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "85145684-dc28-4ddf-965c-dcb53f9a03ba",
   "metadata": {},
   "source": [
    "Once you've logged in to the [Hugging Face hub](https://huggingface.co), please open the [AutoTrain console](https://ui.autotrain.huggingface.co/projects).\n",
    "\n",
    "First, let's give the project a name (```amazon-shoe-reviews-classification```) and pick a task type (```Text Classification (Multi-class)```.\n",
    "\n",
    "Then, we can decide to either let AutoTrain pick the base models to fine-tune, or we can pick a specific model manually. We go with the former (```Automatic```).\n",
    "\n",
    "Finally, we pick the language that base models must support (```English```)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6882131-c864-40a6-ba55-64398e50b4d8",
   "metadata": {},
   "source": [
    "![Create a project](images/09.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8a80063e-21ac-4d9b-9a7d-9a5b0649f62f",
   "metadata": {},
   "source": [
    "The next step is to add a dataset. We can either upload CSV files or point at a dataset hosted on the Hugging Face hub. \n",
    "\n",
    "As we already prepared our dataset and pushed it to the hub at https://huggingface.co/datasets/juliensimon/amazon-shoe-reviews, let's go with the second option and click on ```Browse datasets```."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c277911-122d-4921-bec4-3ea5b886ffa3",
   "metadata": {},
   "source": [
    "![Add files](images/10.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d921544-30ef-46af-ba39-71d83e8c072e",
   "metadata": {},
   "source": [
    "Entering the name of the dataset (```juliensimon/amazon-shoe-reviews```), we first pick the ```train```split, and ask AutoTrain to use it for training.\n",
    "\n",
    "We also map the columns in the dataset to the inputs expected by the model:\n",
    "\n",
    "* ```text``` --> ```text```\n",
    "\n",
    "* ```target``` --> ```labels```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "544ea49a-ea66-40e9-a3bd-f7fa2e80a767",
   "metadata": {},
   "source": [
    "![Add dataset](images/11.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c1edb6a-3788-4def-a970-27395a4dd7a5",
   "metadata": {},
   "source": [
    "![Add training dataset](images/12.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf86061f-c04d-48f7-a1b7-340b9fee1349",
   "metadata": {},
   "source": [
    "We do the same for the test set. We can see that both splits have been properly assigned."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96e22b42-d37e-43f9-ac47-d1fbfc4b0f88",
   "metadata": {},
   "source": [
    "![Add both datasets](images/13.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c5cdef18-04de-4128-8880-9b485636992f",
   "metadata": {},
   "source": [
    "Clicking on ```Go to trainings```, we can pick how many training jobs AutoTrain will run. Let's go with 15."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ac244c6-0293-4d5a-9022-a786ed398257",
   "metadata": {},
   "source": [
    "![Start training](images/14.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8ccf31d-5fe6-42f3-b91d-57efe1267d0b",
   "metadata": {},
   "source": [
    "Once we've clicked on ```Start models training```, AutoTrain loads the datasets and launches the training jobs."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0b075dd-2efa-4e5e-b96b-c36b92f1491d",
   "metadata": {},
   "source": [
    "![Loading data](images/15.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "817767cb-3193-4c22-91e5-3a87c725a2bf",
   "metadata": {},
   "source": [
    "After a few minutes, we can see the jobs running and reporting results. The best model so far is highlighted by a gold star. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fab9ae85-63c9-452d-a6cf-17e870a2ac76",
   "metadata": {},
   "source": [
    "![Job](images/16.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09f2a41d-1e6d-4107-a8dd-8fa8be96db03",
   "metadata": {},
   "source": [
    "Clicking on ```Metrics```, we can see the leaderboard."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8bb408bf-3be9-4189-bb32-7e7a3fca80c3",
   "metadata": {},
   "source": [
    "![Metrics](images/17.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f9c5d0a-b5c7-4219-ba16-524203768ebd",
   "metadata": {},
   "source": [
    "All models are automatically pushed to the hub and marked private."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08823942-1735-4ce2-9d1e-30ee9e1dfaa3",
   "metadata": {},
   "source": [
    "![Best model](images/18.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4fb402d-7383-4d94-9d53-45256d07c98e",
   "metadata": {},
   "source": [
    "Looking at the best model and clicking on ```View on model hub```, we jump to the model page. \n",
    "\n",
    "The model card has been created for us automatically. The model tags tell us that this is a Roberta model.\n",
    "\n",
    "We can also test the model immediately with the inference widget."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a9fc4629-452c-44b1-a3d5-654b0180cca6",
   "metadata": {},
   "source": [
    "![Model page](images/19.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f38cf0cd-02d1-4cfc-92f9-2b784e750c11",
   "metadata": {},
   "source": [
    "Of course, you can use this model just like any other Transformer model\n",
    "\n",
    "```\n",
    "from transformers import AutoTokenizer, AutoModelForSequenceClassification\n",
    "\n",
    "tokenizer = AutoTokenizer.from_pretrained(\"juliensimon/autotrain-amazon-shoe-reviews-classification-1094039779\", use_auth_token=True)\n",
    "\n",
    "model = AutoModelForSequenceClassification.from_pretrained(\"juliensimon/autotrain-amazon-shoe-reviews-classification-1094039779\", use_auth_token=True)\n",
    "```\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5b6a222a-74a9-4712-ad5f-fbc74919a6e4",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
