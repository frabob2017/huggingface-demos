import datasets
import evaluate
import transformers

task_type = "text-classification"

model_id = "juliensimon/distilbert-amazon-shoe-reviews"

dataset_id = "juliensimon/amazon-shoe-reviews"
label_column = "labels"
label_mapping = {
    "LABEL_0": 0,
    "LABEL_1": 1,
    "LABEL_2": 2,
    "LABEL_3": 3,
    "LABEL_4": 4,
}

data = datasets.load_dataset(dataset_id, split="test")
print(data)
metric = evaluate.load("accuracy")
evaluator = evaluate.evaluator(task_type)


def evaluate_pipeline(pipeline):
    results = evaluator.compute(
        model_or_pipeline=pipeline,
        data=data,
        metric=metric,
        label_column=label_column,
        label_mapping=label_mapping,
    )
    return results


print("*** Original model")
classifier = transformers.pipeline(task_type, model_id, device=0)
results = evaluate_pipeline(classifier)
print(results)

print("*** BetterTransformer model")

from optimum.pipelines import pipeline

# Blog post: https://pytorch.org/blog/a-better-transformer-for-fast-transformer-encoder-inference/
# Supported model architectures: https://huggingface.co/docs/optimum/bettertransformer/overview

classifier_bt = pipeline(task_type, model_id, accelerator="bettertransformer", device=0)
results = evaluate_pipeline(classifier_bt)
print(results)
