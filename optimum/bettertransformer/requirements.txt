torch>=1.12.0
transformers>=4.22.1
optimum>=1.5.0
evaluate
scikit-learn
Pillow